`Title : Automated flood mapping system in rice fields`

**Description**

This project uses remote sensing data to detect and map areas that are likely to be flooded in the rice growing fields. It combines the use of Google Earth Engine API to download sentinel-2 imagery, Sentinel-2 imagery to generate flood maps and estimate the extent of flooding in the region. The project also involves generating a dataframe that summarizes the results and provides insights into the spatial and temporal distribution of flooding in the study area.

**Requirements:**

Packages:
- rasterio
- rasterstats
- fiona
- geopandas
- earthpy
- Google Drive access
- API for Google Earth Engine
- Boundaries path: An GEE asset

**Download images**

Inputs:
- API for Google Earth Engine
- Boundaries path: An GEE asset
- Start and end date
- Cloud filter percentage
- Function to apply to image collection (mosaic or median)
- Folder to save images
- Variable of interest (mndwi, cloud, RGB, ndvi, etc.)
Output: 
- sentinel images


**Generate Maps:**

Inputs:
- data_path_mndwi: a list of image file names containing images of water bodies.
- data_path_cloud: a list of image file names containing images of cloud cover.
- rgb_img: a string representing the file path to an RGB image. The RGB refers to the image that completely covers the area and is utilized to calculate the total pixel-based area in hectares.
- savepath: a string representing the file path to save the final output image.

Output: 
- The output is None
- Instead, the function saves an output image to the file path specified by savepath. 
- The saved image is the Flooding map as a TIF file  where each pixel is assigned a value that represents the day of the year when the pixel was detected as flooding and had low cloud cover.

**Generate Dataframe:**

Inputs:
- Season (crossing years or not)
- Output path for the dataframe
- RGB image
- Grid path
- Area name
- Operation (flooding or harvest)
- Year
- Map path

Output: 
- Updated dataframe with flooding information is saved in the output_path


**Author**

Glorie Metsa WOWO


*Scientific Officer - AI and remote sensing at ICRISAT*

**Contributors**

The Team Behind the HEURISTIC Project

