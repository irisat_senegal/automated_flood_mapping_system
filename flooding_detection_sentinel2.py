# -*- coding: utf-8 -*-

'''
@author : Glorie M.
'''

# Download and install packages
import rasterio as rio
import numpy as np
import re
from datetime import datetime
import os
from rasterstats import zonal_stats
import geopandas as gpd
import pandas as pd
from datetime import datetime, date, timedelta
from glob import glob

def from_date_to_doy(date_string):
    date = datetime.strptime(date_string, '%Y-%m-%d')
    return date.timetuple().tm_yday

def create_folder(path):
  """
    Description:
      Create a folder in the given path
    Args:
      path(string): path
    Returns:
  """
  # Check whether the specified path exists or not
  isExist = os.path.exists(path)
  if not isExist:
  # Create a new directory because it does not exist 
    os.makedirs(path)
  else:
    pass

def from_doy_to_date(doy, year):
    """
        Description:
          Convert the day of year into dates
        Args:
          doy(int): the day of year
          year(int): The current
        Returns:
          Returns date in the following format : %Y-%m-%d
    """
    strt_date = date(int(year), 1, 1)
    res_date = strt_date + timedelta(days=int(doy) - 1)
    res = res_date.strftime("%Y-%m-%d")
    return res

def get_images_path(path):
    
    path_ = glob(
      os.path.join(
          "data",
          "vignette-landsat",
          path,
      )
    )
    path_.sort()
    return path_
    
def flooding_module(data_path_mndwi, data_path_cloud,rgb_img, savepath):
    '''
    The objective of this project is to detect and map areas affected by flooding using satellite imagery. The function flooding_module() takes in two lists of image file names as 
    input, data_path_mndwi and data_path_cloud, which contain images of water bodies and cloud cover respectively. The function first sorts the lists of images by date in ascending
     order, and then iterates over the images in the lists, processing them in chronological order from the oldest to the most recent.
    For each pair of images, the function reads the data from the image files using the rasterio library and stores it in the variables im1 and im2. 
    It then applies a series of logical operations to the data to identify pixels that represent water bodies and that have low cloud cover. These pixels are then flagged in a 
    working array arrayimg14 with a value equal to the day of the year of the image. The final output of the function is an image of the same dimensions as the input images, 
    where each pixel is assigned a value that represents the day of the year when the pixel was last detected as a water body and had low cloud cover.
    
    Return
    None
    '''
    data_path_mndwi = get_images_path(data_path_mndwi)
    data_path_cloud = get_images_path(data_path_cloud)
    # Sort data_path_mndwi and data_path_cloud by date in ascending order
    data_path_mndwi = sorted(data_path_mndwi, key=lambda x: re.search(r'mndwi_(\d{4}-\d{2}-\d{2})', x).group(1))
    data_path_cloud = sorted(data_path_cloud, key=lambda x: re.search(r'cloud_(\d{4}-\d{2}-\d{2})', x).group(1))
    # data_path_mndwi.sort()
    # data_path_cloud.sort()
    # print(data_path_mndwi)
    # print('\n')
    # print(data_path_cloud)
    
    # Initialize the image
    rgb =rio.open(rgb_img, driver='Gtiff') 
    working_array = np.copy(rgb.read(1))
    working_array[working_array > 0] = 1
    # Select standing water
    im = rio.open(data_path_mndwi[0], driver='Gtiff').read(1)
    working_array[im>0] = 2
    arrayimg14 = working_array.copy()

    # Iterate over the list of images and cloud cover data
    for mndwi_name, cloud_name in zip(data_path_mndwi[1:], data_path_cloud[1:]):
        mndwi_date = re.search(r'mndwi_(\d{4}-\d{2}-\d{2})', mndwi_name).group(1)
        cloud_date = re.search(r'cloud_(\d{4}-\d{2}-\d{2})', cloud_name).group(1)
        print(mndwi_date, cloud_date)
        if mndwi_date == cloud_date:
            im1 = rio.open(mndwi_name, driver='Gtiff').read(1)
            im2 = rio.open(cloud_name, driver='Gtiff').read(1)
            mask1 = np.logical_and.reduce((im1>0, im2<40)) 
            mask1 = np.logical_and.reduce((mask1, arrayimg14 ==1 ))
            arrayimg14[mask1] = from_date_to_doy(mndwi_date)

    arrayimg14[arrayimg14<3] = 0
    profile = rgb.profile
    with rio.open(savepath, 'w', **profile) as dst:
        dst.write(arrayimg14.astype(rio.uint16), 1)  
    return None

"""# Create dataframes"""

#================================================================
#----------------- POST PROCESSING FUNCTION --------------------
#===============================================================

def update_df(season, output_path, rgb_img, grid_path, area_name, operation, year, map_path):
    """
    This function updates the dataframe of flooding areas and dates.
    
    Parameters:
    - season (str): The season of the year.
    - output_path (str): The path to save the output.
    - rgb_img (str): The path to the RGB image.
    - grid_path (str): The path to the grid shapefile.
    - area_name (str): The name of the area.
    - operation (str): The operation being performed (e.g. flooding).
    - year (int): The year of the images.
    - map_path (str): The path to the map of flooding areas.
    
    Returns:
    - None
    """

    #---------------------------------------------------------------
    def create_proportion( rgb_img, data_path,proportion_path, year):
      '''
      '''
      count_ref_img1 = zonal_stats(data_path,rgb_img,categorical=True)
      stat_df = pd.DataFrame(count_ref_img1)
      geogrid = gpd.read_file (data_path)
      #Consider pixels != NaN
      geogrid ['aoi_area'] =  stat_df[stat_df.columns.to_list()[1:] ].sum(axis=1)

      geogrid ['aoi_area'] = geogrid ['aoi_area']*100*0.0001

      columnsdate = [col for col in geogrid.columns.to_list()[1:]  if str(year) in col]

      gdfproportion = geogrid.copy()
      gdfproportion[columnsdate] = gdfproportion[columnsdate].fillna(0)
    #   gdfproportion[columnsdate] / gdfproportion ['aoi_area']

      for i  in columnsdate :
          gdfproportion [i] = gdfproportion [ i ] / gdfproportion ['aoi_area']

      gdfproportion.to_file( proportion_path)

      return gdfproportion
  
    def cumulative(df,year, savepath):
        '''
        This function calculates the cumulative area of flooding for a given year and saves the result as a csv and geojson file.
        Inputs:
        - df: dataframe containing the flooding data
        - year: the year for which the cumulative flooding area is calculated
        - savepath: the filepath where the result will be saved
        Outputs:
        - df: the modified dataframe with the cumulative flooding area added
        '''
        # Select the columns with the flooding data for the given year
        columnsdate = [col for col in df.columns.to_list() if str(year) in col]
        df = df.copy()
        df = df.fillna(0)
        # Loop through the columns to calculate the cumulative flooding area
        for i in range(len(columnsdate)):
            if i == 0:
                df[columnsdate[i]] = df[columnsdate[i]]
            else:
                df[columnsdate[i]] = df[columnsdate[i - 1]] + df[columnsdate[i]]
        # Save the result as a csv and geojson file
        df.to_csv(savepath)
        gdf1 = gpd.GeoDataFrame(df, geometry='geometry')
        file = savepath.split('.')[0]
        gdf1.to_file(file+'.geojson', driver = 'GeoJSON' )
        return df

    #---------------------------------------------------------------  
    savepath = output_path + "/dataframe/final_result"
    # Create a folder for cumulative data if there is not.
    create_folder(savepath)
    df = ''
    gdfdata= gpd.read_file(grid_path)
    if season == 'crossing_years': # Rainy season in Senegal ,We assume the rainy is from August to ...

    # #   count_ref_img1 = zonal_stats(grid_path, output_path + '/rasters/' + operation+'map.tif' ,categorical=True )
        count_ref_img1 = zonal_stats(grid_path, map_path ,categorical=True )
        stat_df1 = pd.DataFrame(count_ref_img1)
        coldoy = stat_df1.columns [1:]

        coldate =  [from_doy_to_date(x, year) if x > 111 else from_doy_to_date(x + 256, year)  for x in coldoy  ]  #A year has a maximum of 366 days, so 366% 256 = 110
     
        stat_df1 = stat_df1.rename( columns = dict(zip(coldoy ,  coldate)) )
        coldate.sort()
        stat_df1 =  stat_df1[coldate] * 100*0.0001
        df = stat_df1 
        df[gdfdata.columns] = gdfdata
      
    else : 
        count_ref_img1 = zonal_stats(grid_path, map_path ,categorical=True )
        stat_df1 = pd.DataFrame(count_ref_img1)
        coldoy = stat_df1.columns [1:]

        coldate =  [from_doy_to_date(x, year)  for x in coldoy  ]  #A year has a maximum of 366 days, so 366% 256 = 110

      
        stat_df1 = stat_df1.rename( columns = dict(zip(coldoy ,  coldate)) )
        coldate.sort()
        stat_df1 =  stat_df1[coldate] * 100*0.0001
      
        df = stat_df1
        df[gdfdata.columns] = gdfdata
      

    #-------------------------------------------------------------------------------------
    # df =  stat_df1  
    # rgb_img = '/content/drive/MyDrive/earthengine/NEWTEST/rgb_2021-01-02.tif'
 
    # Path where the proportion will be saved
    proportion_path = output_path + "/dataframe/flooding_proportion.geojson"
    #-----------

    gdfprop = create_proportion ( rgb_img, grid_path, proportion_path, year)

    gdfprop [coldate] = df[coldate]
    
    for i  in coldate :
          gdfprop [i] = gdfprop [ i ] / gdfprop ['aoi_area']
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    #3.---------------------- For the cumulative ------------------------------

    # savepath = output_path + "/dataframe/cumulative"
    savepath = output_path + "/dataframe/final_result"
    # Create a folder for cumulative data if there is not.
    create_folder(savepath)
    # # Cumulative of the initial flooding output


    columnsdate = [col for col in df.columns.to_list() if str(year) in col]

    df['aoi_area'] = gdfprop ['aoi_area']
    
    df ['total_'+operation+'_area'] =  df[columnsdate].sum(axis=1)
    
    
    maxValueIndex1 = df[columnsdate].idxmax(axis=1)
    df [operation+'_date'] = maxValueIndex1 
    
    gdfprop  ['total_'+operation+'_area'] =  df[columnsdate].sum(axis=1)
    
    gdfprop [operation+'_date'] = df [operation+'_date']
    

    col_ = [col for col in df.columns.to_list() if str(year) not in col] 
    
    
    colss = col_ + columnsdate #[1:]



    df11 = cumulative (df[colss], year,savepath+'/' + operation + '_' + area_name + str(year)+  '.csv')

    # df11 = cumulative (df[colss], year,savepath+'/' + operation + '_'  + area_name + str(year)+  '.geojson' )

    # # Cumulative of the proportion
    df22 = cumulative (gdfprop [colss], year, savepath+'/' + operation + '_proportion_' + area_name + str(year)+  '.csv')

